//JavaClasses/src/test/java/mattrixwv/TestStopwatch.java
//Matthew Ellison
// Created: 06-07-20
//Modified: 04-13-23
//This class holds many algorithms that I have found it useful to keep around
//As such all of the functions in here are static and meant to be used as stand alone functions
/*
Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


package com.mattrixwv;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import com.mattrixwv.exceptions.InvalidResult;


public class TestStopwatch{
	private static final Integer NUM_TO_RUN = 100000;
	private static final Double ALLOWANCE = .0000000001;

	@Test
	public void testStartStop(){
		Stopwatch timer = new Stopwatch();
		timer.start();
		timer.stop();
		assertNotNull(timer.toString());
		//If it gets to here without throwing an exception everything went well
	}

	@Test
	public void testConversion() throws InvalidResult{
		Stopwatch timer = new Stopwatch();
		Integer sum = 0;
		//Start the timer
		timer.start();
		//Do something to run some time
		for(int cnt = 0;cnt < NUM_TO_RUN;++cnt){
			sum += cnt;
		}
		//Stop the timer
		timer.stop();
		//Assert something so the sum isn't ignored during compile
		assertNotEquals(sum, Integer.valueOf(0), "You really messed up");
		//Check that the different resolutions work out correctly
		Double nano = timer.getNano();
		assertEquals(timer.getMicro(), (nano / 1000D), ALLOWANCE, "Micro resolution test failed");
		assertEquals(timer.getMilli(), (nano / 1000000D), ALLOWANCE, "Milli resolution test failed");
		assertEquals(timer.getSecond(), (nano / 1000000000D), ALLOWANCE, "Second resolution test failed");
		assertEquals(timer.getMinute(), (nano / 60000000000D), ALLOWANCE, "Minute resolution test failed");
		assertEquals(timer.getHour(), (nano / 3600000000000D), ALLOWANCE, "Hour resolution test failed");
	}

	@Test
	public void testStringConversion() throws InvalidResult{
		//Test nanoseconds
		String results = Stopwatch.getStr(1.0);
		assertEquals("1.000 nanoseconds", results);
		//Test microseconds
		results = Stopwatch.getStr(1.0e3);
		assertEquals("1.000 microseconds", results);
		//Test milliseconds
		results = Stopwatch.getStr(1.0e6);
		assertEquals("1.000 milliseconds", results);
		results = Stopwatch.getStr(1.2e8);
		assertEquals("120.000 milliseconds", results);
		//Test seconds
		results = Stopwatch.getStr(1.0e9);
		assertEquals("1.000 seconds", results);
		//Test minutes
		results = Stopwatch.getStr(1.0e12);
		assertEquals("16.667 minutes", results);
		//Test hours
		results = Stopwatch.getStr(1.0e13);
		assertEquals("2.778 hours", results);

		//Test error
		assertThrows(InvalidResult.class, () -> {
			Stopwatch.getStr(-1.0);
		});
	}

	@Test
	public void testErrors(){
		Stopwatch timer = new Stopwatch();
		assertEquals(0.0, timer.getNano(), "Stopwatch failed not started test");

		timer.stop();
		assertEquals(0.0, timer.getNano(), "Stopwatch failed stop before start test");

		timer.start();
		assertNotEquals(0.0, timer.getNano(), "Stopwatch failed not stopped test");

		timer.reset();
		assertEquals(0.0, timer.getNano(), "Stopwatch failed reset test");
	}
}
