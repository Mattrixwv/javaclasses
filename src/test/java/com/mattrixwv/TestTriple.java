//JavaClasses/src/test/java/com/mattrixwv/TestTriple.java
//Mattrixwv
// Created: 08-20-22
//Modified: 04-13-23
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class TestTriple{
	private Triple<Long, Long, Long> triple;


	@BeforeEach
	public void setup(){
		triple = new Triple<>(1L, 2L, 3L);
	}


	@Test
	public void testConstructor(){
		assertEquals(1L, triple.getA());
		assertEquals(2L, triple.getB());
		assertEquals(3L, triple.getC());
	}

	@Test
	public void testGetters(){
		assertEquals(1L, triple.getA());
		assertEquals(2L, triple.getB());
		assertEquals(3L, triple.getC());
	}

	@Test
	public void testEquals(){
		Triple<Long, Long, Long> triple2 = new Triple<>(1L, 2L, 3L);
		Triple<Long, Long, Long> triple3 = new Triple<>(3L, 2L, 3L);
		Triple<Long, Long, Long> triple4 = new Triple<>(1L, 3L, 3L);
		Triple<Long, Long, Long> triple5 = new Triple<>(1L, 2L, 1L);
		Triple<Long, Long, Long> triple6 = new Triple<>(2L, 1L, 3L);
		Triple<Long, Long, Long> triple7 = new Triple<>(3L, 2L, 1L);
		Triple<Long, Long, Long> triple8 = new Triple<>(1L, 3L, 2L);
		Triple<Long, Long, Long> triple9 = new Triple<>(3L, 1L, 2L);

		assertEquals(triple, triple);
		assertEquals(triple, triple2);
		assertNotEquals(triple, triple3);
		assertNotEquals(triple, triple4);
		assertNotEquals(triple, triple5);
		assertNotEquals(triple, triple6);
		assertNotEquals(triple, triple7);
		assertNotEquals(triple, triple8);
		assertNotEquals(triple, triple9);
		assertNotEquals(triple, 1L);
	}

	@Test
	public void testHashCode(){
		assertEquals(Long.hashCode(1) + Long.hashCode(2) * Long.hashCode(3), triple.hashCode());
	}

	@Test
	public void testToString(){
		assertEquals("[1, 2, 3]", triple.toString());
	}
}
