//JavaClasses/src/test/java/mattrixwv/TestArrayAlgorithms.java
//Matthew Ellison
// Created: 07-03-21
//Modified: 06-26-22
//This class contains tests for my array algorithms
/*
	Copyright (C) 2022  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv;


import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;


public class TestArrayAlgorithms{
	@Test
	public void testGetSum(){
		//Test 1
		int correctAnswer = 0;
		List<Integer> numbers = new ArrayList<>();
		int answer = ArrayAlgorithms.getSum(numbers);
		assertEquals(correctAnswer, answer, "getSum int 1 failed");
		//Test 2
		correctAnswer = 118;
		numbers = Arrays.asList(2, 2, 3, 3, 4, 4, 100);
		answer = ArrayAlgorithms.getSum(numbers);
		assertEquals(correctAnswer, answer, "getSum int 2 failed");

		//Test 3
		long longCorrectAnswer = 0;
		List<Long> longNumbers = new ArrayList<>();
		long longAnswer = ArrayAlgorithms.getLongSum(longNumbers);
		assertEquals(longCorrectAnswer, longAnswer, "getSum long 1 failed");
		//Test 4
		longCorrectAnswer = 118;
		longNumbers = Arrays.asList(2L, 2L, 3L, 3L, 4L, 4L, 100L);
		longAnswer = ArrayAlgorithms.getLongSum(longNumbers);
		assertEquals(longCorrectAnswer, longAnswer, "getSum long 2 failed");

		//Test 5
		BigInteger bigCorrectAnswer = BigInteger.ZERO;
		List<BigInteger> bigNumbers = new ArrayList<>();
		BigInteger bigAnswer = ArrayAlgorithms.getBigSum(bigNumbers);
		assertEquals(bigCorrectAnswer, bigAnswer, "getSum BigInteger 1 failed");
		//Test 6
		bigCorrectAnswer = BigInteger.valueOf(118);
		bigNumbers = Arrays.asList(BigInteger.valueOf(2), BigInteger.valueOf(2), BigInteger.valueOf(3), BigInteger.valueOf(3), BigInteger.valueOf(4), BigInteger.valueOf(4), BigInteger.valueOf(100));
		bigAnswer = ArrayAlgorithms.getBigSum(bigNumbers);
		assertEquals(bigCorrectAnswer, bigAnswer, "getSum BigInteger 2 failed");
	}

	@Test
	public void testGetProd(){
		//Test 1
		int correctAnswer = 0;
		List<Integer> numbers = new ArrayList<>();
		int answer = ArrayAlgorithms.getProd(numbers);
		assertEquals(correctAnswer, answer, "getProd int 1 failed");
		//Test 2
		correctAnswer = 57600;
		numbers = Arrays.asList(2, 2, 3, 3, 4, 4, 100);
		answer = ArrayAlgorithms.getProd(numbers);
		assertEquals(correctAnswer, answer, "getProd int 2 failed");

		//Test 3
		long longCorrectAnswer = 0;
		List<Long> longNumbers = new ArrayList<>();
		long longAnswer = ArrayAlgorithms.getLongProd(longNumbers);
		assertEquals(longCorrectAnswer, longAnswer, "getProd long 1 failed");
		//Test 4
		longCorrectAnswer = 57600L;
		longNumbers = Arrays.asList(2L, 2L, 3L, 3L, 4L, 4L, 100L);
		longAnswer = ArrayAlgorithms.getLongProd(longNumbers);
		assertEquals(longCorrectAnswer, longAnswer, "getProd long 2 failed");

		//Test 5
		BigInteger bigCorrectAnswer = BigInteger.ZERO;
		List<BigInteger> bigNumbers = new ArrayList<BigInteger>();
		BigInteger bigAnswer = ArrayAlgorithms.getBigProd(bigNumbers);
		assertEquals(bigCorrectAnswer, bigAnswer, "getProd BigInteger 1 failed");
		//Test 6
		bigCorrectAnswer = BigInteger.valueOf(57600);
		bigNumbers = Arrays.asList(BigInteger.valueOf(2), BigInteger.valueOf(2), BigInteger.valueOf(3), BigInteger.valueOf(3), BigInteger.valueOf(4), BigInteger.valueOf(4), BigInteger.valueOf(100));
		bigAnswer = ArrayAlgorithms.getBigProd(bigNumbers);
		assertEquals(bigCorrectAnswer, bigAnswer, "getProd BigInteger 2 failed");
	}

	@Test
	public void testPrintList(){
		//Test 1
		List<Integer> nums = new ArrayList<Integer>();
		String correctAnswer = "[]";
		String answer = ArrayAlgorithms.printList(nums);
		assertEquals(correctAnswer, answer, "printList<Integer> 1 failed");
		//Test 2
		nums = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
		correctAnswer = "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]";
		answer = ArrayAlgorithms.printList(nums);
		assertEquals(correctAnswer, answer, "printList<Integer> 2 failed");
		//Test 3
		nums = Arrays.asList(-3, -2, -1, 0, 1, 2, 3);
		correctAnswer = "[-3, -2, -1, 0, 1, 2, 3]";
		answer = ArrayAlgorithms.printList(nums);
		assertEquals(correctAnswer, answer, "printList<Integer> 3 failed");

		//Test 4
		List<String> strings = new ArrayList<String>();
		correctAnswer = "[]";
		answer = ArrayAlgorithms.printList(strings);
		assertEquals(correctAnswer, answer, "printList<String> 1 failed");
		//Test 5
		strings = Arrays.asList("A", "B", "C");
		correctAnswer = "[A, B, C]";
		answer = ArrayAlgorithms.printList(strings);
		assertEquals(correctAnswer, answer, "printList<String> 2 failed");
		//Test 6
		strings = new ArrayList<String>(Arrays.asList("abc", "def", "ghi"));
		correctAnswer = "[abc, def, ghi]";
		answer = ArrayAlgorithms.printList(strings);
		assertEquals(correctAnswer, answer, "printList<String> 3 failed");
	}
}
