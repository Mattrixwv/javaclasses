//JavaClasses/src/test/java/mattrixwv/TestStringAlgorithms.java
//Matthew Ellison
// Created: 07-03-21
//Modified: 04-13-23
//This class contains tests for my number algorithms
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv;


import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;


public class TestStringAlgorithms{
	@Test
	public void testGetPermutations(){
		//Test 1
		String permString = "012";
		List<String> correctAnswer = Arrays.asList("012", "021", "102", "120", "201", "210");
		List<String> answer = StringAlgorithms.getPermutations(permString);
		assertEquals(correctAnswer, answer, "getPermutations failed");

		//Test 2
		permString = "012";
		correctAnswer = new ArrayList<>();
		answer = StringAlgorithms.getPermutations(permString, 4);
		assertEquals(correctAnswer, answer);

		//Test 3
		permString = "012";
		correctAnswer = new ArrayList<>();
		answer = StringAlgorithms.getPermutations(permString, -1);
		assertEquals(correctAnswer, answer);
	}
	@Test
	public void testFindNumOccurrence(){
		//Test 1
		String testString = "abcdefgdd";
		char testChar = 'a';
		long correctAnswer = 1;
		long answer = StringAlgorithms.findNumOccurrence(testString, testChar);
		assertEquals(correctAnswer, answer, "FindNumOccurrence 1 failed");
		//Test 2
		testChar = 'd';
		correctAnswer = 3;
		answer = StringAlgorithms.findNumOccurrence(testString, testChar);
		assertEquals(correctAnswer, answer, "FindNumOccurrence 2 failed");
		//Test 3
		testChar = 'h';
		correctAnswer = 0;
		answer = StringAlgorithms.findNumOccurrence(testString, testChar);
		assertEquals(correctAnswer, answer, "FindNumOccurrence 3 failed");
	}
	@Test
	public void testIsPalindrome(){
		//Test 1
		String str = "101";
		boolean correctAnswer = true;
		boolean answer = StringAlgorithms.isPalindrome(str);
		assertEquals(correctAnswer, answer, "isPalindrome 1 failed");
		//Test 2
		str = "100";
		correctAnswer = false;
		answer = StringAlgorithms.isPalindrome(str);
		assertEquals(correctAnswer, answer, "isPalindrome 2 failed");
		//Test 3
		str = "";
		correctAnswer = true;
		answer = StringAlgorithms.isPalindrome(str);
		assertEquals(correctAnswer, answer, "isPalindrome 3 failed");
	}
	@Test
	public void testIsPandigital(){
		//Test 1
		String num = "123456789";
		boolean correctAnswer = true;
		boolean answer = StringAlgorithms.isPandigital(num);
		assertEquals(correctAnswer, answer, "isPandigital 1 failed");

		//Test 2
		num = "123";
		correctAnswer = true;
		answer = StringAlgorithms.isPandigital(num, '1', '3');
		assertEquals(correctAnswer, answer, "isPandigital 2 failed");

		//Test 3
		num = "123";
		correctAnswer = false;
		answer = StringAlgorithms.isPandigital(num);
		assertEquals(correctAnswer, answer, "isPandigital 3 failed");

		//Test 4
		num = "123";
		correctAnswer = false;
		answer = StringAlgorithms.isPandigital(num, '3', '1');
		assertEquals(correctAnswer, answer, "isPandigital 4 failed");

		//Test 5
		num = "1";
		correctAnswer = true;
		answer = StringAlgorithms.isPandigital(num, '1', '1');
		assertEquals(correctAnswer, answer, "isPandigital 5 failed");

		//Test 6
		num = "112";
		correctAnswer = false;
		answer = StringAlgorithms.isPandigital(num, '1', '3');
		assertEquals(correctAnswer, answer, "isPandigital 6 failed");
	}
}
