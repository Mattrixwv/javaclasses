//JavaClasses/src/test/java/mattrixwv/TestNumberAlgorithms.java
//Matthew Ellison
// Created: 07-03-21
//Modified: 04-13-23
//This class contains tests for my number algorithms
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigInteger;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.mattrixwv.exceptions.InvalidResult;


public class TestNumberAlgorithms{
	@Test
	public void testGetPrimes(){
		//Test 1
		List<Integer> correctAnswer = new ArrayList<>();
		int topNum = 1;
		List<Integer> answer = NumberAlgorithms.getPrimes(topNum);
		assertEquals(correctAnswer, answer, "getPrimes int 1 failed");
		//Test 2
		correctAnswer = Arrays.asList(2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97);
		topNum = 100;
		answer = NumberAlgorithms.getPrimes(topNum);
		assertEquals(correctAnswer, answer, "getPrimes int 2 failed");

		//Test 3
		List<Long> longCorrectAnswer = new ArrayList<>();
		long longTopNum = 0;
		List<Long> longAnswer = NumberAlgorithms.getPrimes(longTopNum);
		assertEquals(longCorrectAnswer, longAnswer, "getPrimes long 1 failed");
		//Test 4
		longCorrectAnswer = Arrays.asList(2L, 3L, 5L, 7L, 11L, 13L, 17L, 19L, 23L, 29L, 31L, 37L, 41L, 43L, 47L, 53L, 59L, 61L, 67L, 71L, 73L, 79L, 83L, 89L, 97L);
		longTopNum = 100;
		longAnswer = NumberAlgorithms.getPrimes(longTopNum);
		assertEquals(longCorrectAnswer, longAnswer, "getPrimes long 2 failed");

		//Test 5
		List<BigInteger> bigCorrectAnswer = new ArrayList<>();
		BigInteger bigTopNum = BigInteger.ZERO;
		List<BigInteger> bigAnswer = NumberAlgorithms.getPrimes(bigTopNum);
		assertEquals(bigCorrectAnswer, bigAnswer, "getPrimes BigInteger 1 failed");
		//Test 6
		bigCorrectAnswer = Arrays.asList(BigInteger.valueOf(2), BigInteger.valueOf(3), BigInteger.valueOf(5), BigInteger.valueOf(7), BigInteger.valueOf(11), BigInteger.valueOf(13), BigInteger.valueOf(17), BigInteger.valueOf(19), BigInteger.valueOf(23), BigInteger.valueOf(29), BigInteger.valueOf(31), BigInteger.valueOf(37), BigInteger.valueOf(41), BigInteger.valueOf(43), BigInteger.valueOf(47), BigInteger.valueOf(53), BigInteger.valueOf(59), BigInteger.valueOf(61), BigInteger.valueOf(67), BigInteger.valueOf(71), BigInteger.valueOf(73), BigInteger.valueOf(79), BigInteger.valueOf(83), BigInteger.valueOf(89), BigInteger.valueOf(97));
		bigTopNum = BigInteger.valueOf(100);
		bigAnswer = NumberAlgorithms.getPrimes(bigTopNum);
		assertEquals(bigCorrectAnswer, bigAnswer, "getPrimes BigInteger 2 failed");
	}

	@Test
	public void testGetNumPrimes(){
		//Test 1
		List<Integer> correctAnswer = new ArrayList<>();
		int numPrimes = 0;
		List<Integer> answer = NumberAlgorithms.getNumPrimes(numPrimes);
		assertEquals(correctAnswer, answer, "getNumPrimes int 1 failed");
		//Test 2
		correctAnswer = Arrays.asList(2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97);
		numPrimes = 25;
		answer = NumberAlgorithms.getNumPrimes(numPrimes);
		assertEquals(correctAnswer, answer, "getNumPrimes int 2 failed");

		//Test 3
		List<Long> longCorrectAnswer = new ArrayList<>();
		long longNumPrimes = 0;
		List<Long> longAnswer = NumberAlgorithms.getNumPrimes(longNumPrimes);
		assertEquals(longCorrectAnswer, longAnswer, "getNumPrimes long 1 failed");
		//Test 4
		longCorrectAnswer = Arrays.asList(2L, 3L, 5L, 7L, 11L, 13L, 17L, 19L, 23L, 29L, 31L, 37L, 41L, 43L, 47L, 53L, 59L, 61L, 67L, 71L, 73L, 79L, 83L, 89L, 97L);
		longNumPrimes = 25;
		longAnswer = NumberAlgorithms.getNumPrimes(longNumPrimes);
		assertEquals(longCorrectAnswer, longAnswer, "getNumPrimes long 2 failed");

		//Test 5
		List<BigInteger> bigCorrectAnswer = new ArrayList<>();
		BigInteger bigNumPrimes = BigInteger.ZERO;
		List<BigInteger> bigAnswer = NumberAlgorithms.getNumPrimes(bigNumPrimes);
		assertEquals(bigCorrectAnswer, bigAnswer, "getNumPrimes BigInteger 1 failed");
		//Test 6
		bigCorrectAnswer = Arrays.asList(BigInteger.valueOf(2), BigInteger.valueOf(3), BigInteger.valueOf(5), BigInteger.valueOf(7), BigInteger.valueOf(11), BigInteger.valueOf(13), BigInteger.valueOf(17), BigInteger.valueOf(19), BigInteger.valueOf(23), BigInteger.valueOf(29), BigInteger.valueOf(31), BigInteger.valueOf(37), BigInteger.valueOf(41), BigInteger.valueOf(43), BigInteger.valueOf(47), BigInteger.valueOf(53), BigInteger.valueOf(59), BigInteger.valueOf(61), BigInteger.valueOf(67), BigInteger.valueOf(71), BigInteger.valueOf(73), BigInteger.valueOf(79), BigInteger.valueOf(83), BigInteger.valueOf(89), BigInteger.valueOf(97));
		bigNumPrimes = BigInteger.valueOf(25);
		bigAnswer = NumberAlgorithms.getNumPrimes(bigNumPrimes);
		assertEquals(bigCorrectAnswer, bigAnswer, "getNumPrimes BigInteger 2 failed");
	}

	@Test
	public void testIsPrime(){
		//Test 1
		int num = 4;
		boolean correctAnswer = false;
		boolean answer = NumberAlgorithms.isPrime(num);
		assertEquals(correctAnswer, answer);
		//Test 2
		num = 15;
		correctAnswer = false;
		answer = NumberAlgorithms.isPrime(num);
		assertEquals(correctAnswer, answer);
		//Test 3
		num = 97;
		correctAnswer = true;
		answer = NumberAlgorithms.isPrime(num);
		assertEquals(correctAnswer, answer);
		//Test 4
		num = 1;
		correctAnswer = false;
		answer = NumberAlgorithms.isPrime(num);
		assertEquals(correctAnswer, answer);
		//Test 5
		num = 2;
		correctAnswer = true;
		answer = NumberAlgorithms.isPrime(num);
		assertEquals(correctAnswer, answer);
		//Test 6
		num = 49;
		correctAnswer = false;
		answer = NumberAlgorithms.isPrime(num);
		assertEquals(correctAnswer, answer);
		//Test 7
		num = 55;
		correctAnswer = false;
		answer = NumberAlgorithms.isPrime(num);
		assertEquals(correctAnswer, answer);

		//Test 8
		long longNum = 4L;
		correctAnswer = false;
		answer = NumberAlgorithms.isPrime(longNum);
		assertEquals(correctAnswer, answer);
		//Test 9
		longNum = 15L;
		correctAnswer = false;
		answer = NumberAlgorithms.isPrime(longNum);
		assertEquals(correctAnswer, answer);
		//Test 10
		longNum = 97L;
		correctAnswer = true;
		answer = NumberAlgorithms.isPrime(longNum);
		assertEquals(correctAnswer, answer);
		//Test 11
		longNum = 1L;
		correctAnswer = false;
		answer = NumberAlgorithms.isPrime(longNum);
		assertEquals(correctAnswer, answer);
		//Test 12
		longNum = 2L;
		correctAnswer = true;
		answer = NumberAlgorithms.isPrime(longNum);
		assertEquals(correctAnswer, answer);
		//Test 13
		longNum = 49L;
		correctAnswer = false;
		answer = NumberAlgorithms.isPrime(longNum);
		assertEquals(correctAnswer, answer);
		//Test 14
		longNum = 55L;
		correctAnswer = false;
		answer = NumberAlgorithms.isPrime(longNum);
		assertEquals(correctAnswer, answer);

		//Test 15
		BigInteger bigNum = BigInteger.valueOf(4);
		correctAnswer = false;
		answer = NumberAlgorithms.isPrime(bigNum);
		assertEquals(correctAnswer, answer);
		//Test 16
		bigNum = BigInteger.valueOf(15);
		correctAnswer = false;
		answer = NumberAlgorithms.isPrime(bigNum);
		assertEquals(correctAnswer, answer);
		//Test 17
		bigNum = BigInteger.valueOf(97);
		correctAnswer = true;
		answer = NumberAlgorithms.isPrime(bigNum);
		assertEquals(correctAnswer, answer);
		//Test 18
		bigNum = BigInteger.valueOf(1);
		correctAnswer = false;
		answer = NumberAlgorithms.isPrime(bigNum);
		assertEquals(correctAnswer, answer);
		//Test 19
		bigNum = BigInteger.valueOf(2);
		correctAnswer = true;
		answer = NumberAlgorithms.isPrime(bigNum);
		assertEquals(correctAnswer, answer);
		//Test 20
		bigNum = BigInteger.valueOf(49);
		correctAnswer = false;
		answer = NumberAlgorithms.isPrime(bigNum);
		assertEquals(correctAnswer, answer);
		//Test 21
		bigNum = BigInteger.valueOf(55);
		correctAnswer = false;
		answer = NumberAlgorithms.isPrime(bigNum);
		assertEquals(correctAnswer, answer);
	}

	@Test
	public void testGetFactors() throws InvalidResult{
		//Test 1
		List<Integer> correctAnswer = Arrays.asList(2, 2, 5, 5);
		int number = 100;
		List<Integer> answer = NumberAlgorithms.getFactors(number);
		assertEquals(correctAnswer, answer);
		//Test 2
		correctAnswer = Arrays.asList(2, 7, 7);
		number = 98;
		answer = NumberAlgorithms.getFactors(number);
		assertEquals(correctAnswer, answer);
		//Test 3
		correctAnswer = Arrays.asList(7);
		number = 7;
		answer = NumberAlgorithms.getFactors(number);
		assertEquals(correctAnswer, answer);
		//Test 4
		correctAnswer = Arrays.asList(2, 5);
		number = 10;
		answer = NumberAlgorithms.getFactors(number);
		assertEquals(correctAnswer, answer);


		//Test 5
		List<Long> longCorrectAnswer = Arrays.asList(2L, 2L, 5L, 5L);
		long longNumber = 100L;
		List<Long> longAnswer = NumberAlgorithms.getFactors(longNumber);
		assertEquals(longCorrectAnswer, longAnswer);
		//Test 6
		longCorrectAnswer = Arrays.asList(2L, 7L, 7L);
		longNumber  = 98;
		longAnswer = NumberAlgorithms.getFactors(longNumber);
		assertEquals(longCorrectAnswer, longAnswer);
		//Test 7
		longCorrectAnswer = Arrays.asList(7L);
		longNumber = 7;
		longAnswer = NumberAlgorithms.getFactors(longNumber);
		assertEquals(longCorrectAnswer, longAnswer);
		//Test 8
		longCorrectAnswer = Arrays.asList(2L, 5L);
		longNumber = 10L;
		longAnswer = NumberAlgorithms.getFactors(longNumber);
		assertEquals(longCorrectAnswer, longAnswer);


		//Test 9
		List<BigInteger> bigCorrectAnswer = Arrays.asList(BigInteger.TWO, BigInteger.TWO, BigInteger.valueOf(5), BigInteger.valueOf(5));
		BigInteger bigNumber = BigInteger.valueOf(100);
		List<BigInteger> bigAnswer = NumberAlgorithms.getFactors(bigNumber);
		assertEquals(bigCorrectAnswer, bigAnswer);
		//Test 10
		bigCorrectAnswer = Arrays.asList(BigInteger.valueOf(2), BigInteger.valueOf(7), BigInteger.valueOf(7));
		bigNumber = BigInteger.valueOf(98);
		bigAnswer = NumberAlgorithms.getFactors(bigNumber);
		assertEquals(bigCorrectAnswer, bigAnswer);
		//Test 11
		bigCorrectAnswer = Arrays.asList(BigInteger.valueOf(7));
		bigNumber = BigInteger.valueOf(7);
		bigAnswer = NumberAlgorithms.getFactors(bigNumber);
		assertEquals(bigCorrectAnswer, bigAnswer);
		//Test 12
		bigCorrectAnswer = Arrays.asList(BigInteger.TWO, BigInteger.valueOf(5));
		bigNumber = BigInteger.valueOf(10);
		bigAnswer = NumberAlgorithms.getFactors(bigNumber);
		assertEquals(bigCorrectAnswer, bigAnswer);
	}

	@Test
	public void testGetDivisors(){
		//Test 1
		List<Integer> correctAnswer = Arrays.asList(1, 2, 4, 5, 10, 20, 25, 50, 100);
		int topNum = 100;
		List<Integer> answer = NumberAlgorithms.getDivisors(topNum);
		assertEquals(correctAnswer, answer, "getDivisors int 1 failed");
		//Test 2
		correctAnswer = Arrays.asList(1, 2, 3, 6);
		topNum = 6;
		answer = NumberAlgorithms.getDivisors(topNum);
		assertEquals(correctAnswer, answer, "getDivisors int 2 failed");
		//Test 3
		correctAnswer = new ArrayList<>();
		topNum = -1;
		answer = NumberAlgorithms.getDivisors(topNum);
		assertEquals(correctAnswer, answer, "getDivisors int 3 failed");

		//Test 4
		List<Long> longCorrectAnswer = Arrays.asList(1L, 2L, 4L, 5L, 10L, 20L, 25L, 50L, 100L);
		long longTopNum = 100;
		List<Long> longAnswer = NumberAlgorithms.getDivisors(longTopNum);
		assertEquals(longCorrectAnswer, longAnswer, "getDivisors long 1 failed");
		//Test 5
		longCorrectAnswer = Arrays.asList(1L, 2L, 3L, 6L);
		longTopNum = 6;
		longAnswer = NumberAlgorithms.getDivisors(longTopNum);
		assertEquals(longCorrectAnswer, longAnswer, "getDivisors long 2 failed");
		//Test 6
		longCorrectAnswer = new ArrayList<>();
		longTopNum = -1;
		longAnswer = NumberAlgorithms.getDivisors(longTopNum);
		assertEquals(longCorrectAnswer, longAnswer, "getDivisors long 3 failed");

		//Test 7
		List<BigInteger> bigCorrectAnswer = Arrays.asList(BigInteger.valueOf(1), BigInteger.valueOf(2), BigInteger.valueOf(4), BigInteger.valueOf(5), BigInteger.valueOf(10), BigInteger.valueOf(20), BigInteger.valueOf(25), BigInteger.valueOf(50), BigInteger.valueOf(100));
		BigInteger bigTopNum = BigInteger.valueOf(100);
		List<BigInteger> bigAnswer = NumberAlgorithms.getDivisors(bigTopNum);
		assertEquals(bigCorrectAnswer, bigAnswer, "getDivisors BigInteger 1 failed");
		//Test 8
		bigCorrectAnswer = Arrays.asList(BigInteger.ONE, BigInteger.TWO, BigInteger.valueOf(3), BigInteger.valueOf(6));
		bigTopNum = BigInteger.valueOf(6);
		bigAnswer = NumberAlgorithms.getDivisors(bigTopNum);
		assertEquals(bigCorrectAnswer, bigAnswer, "getDivisors BigInteger 2 failed");
		//Test 9
		bigCorrectAnswer = new ArrayList<>();
		bigTopNum = BigInteger.valueOf(-1);
		bigAnswer = NumberAlgorithms.getDivisors(bigTopNum);
		assertEquals(bigCorrectAnswer, bigAnswer, "getDivisors BigInteger 3 failed");
	}

	@Test
	public void testGetFib(){
		//Test 1
		int correctAnswer = 0;
		int number = 0;
		int answer = NumberAlgorithms.getFib(number);
		assertEquals(correctAnswer, answer, "getFib int 1 failed");
		//Test 2
		correctAnswer = 144;
		number = 12;
		answer = NumberAlgorithms.getFib(number);
		assertEquals(correctAnswer, answer, "getFib int 2 failed");
		//Test 3
		correctAnswer = 6765;
		number = 20;
		answer = NumberAlgorithms.getFib(number);
		assertEquals(correctAnswer, answer, "getFib int 3 failed");

		//Test 4
		long longCorrectAnswer = 0;
		long longNumber = 0;
		long longAnswer = NumberAlgorithms.getFib(longNumber);
		assertEquals(longCorrectAnswer, longAnswer, "getFib long 1 failed");
		//Test 5
		longCorrectAnswer = 144;
		longNumber = 12;
		longAnswer = NumberAlgorithms.getFib(longNumber);
		assertEquals(longCorrectAnswer, longAnswer, "getFib long 2 failed");
		//Test 6
		longCorrectAnswer = 6765;
		longNumber = 20;
		longAnswer = NumberAlgorithms.getFib(longNumber);
		assertEquals(longCorrectAnswer, longAnswer, "getFib long 3 failed");

		//Test 7
		BigInteger bigCorrectAnswer = BigInteger.ZERO;
		BigInteger bigNumber = BigInteger.ZERO;
		BigInteger bigAnswer = NumberAlgorithms.getFib(bigNumber);
		assertEquals(bigCorrectAnswer, bigAnswer, "getFib BigInteger 1 failed");
		//Test 8
		bigCorrectAnswer = BigInteger.valueOf(144);
		bigNumber = BigInteger.valueOf(12);
		bigAnswer = NumberAlgorithms.getFib(bigNumber);
		assertEquals(bigCorrectAnswer, bigAnswer, "getFib BigInteger 2 failed");
		//Test 9
		bigCorrectAnswer = BigInteger.valueOf(6765);
		bigNumber = BigInteger.valueOf(20);
		bigAnswer = NumberAlgorithms.getFib(bigNumber);
		assertEquals(bigCorrectAnswer, bigAnswer, "getFib BigInteger 3 failed");
		//Test 10
		bigCorrectAnswer = new BigInteger("1070066266382758936764980584457396885083683896632151665013235203375314520604694040621889147582489792657804694888177591957484336466672569959512996030461262748092482186144069433051234774442750273781753087579391666192149259186759553966422837148943113074699503439547001985432609723067290192870526447243726117715821825548491120525013201478612965931381792235559657452039506137551467837543229119602129934048260706175397706847068202895486902666185435124521900369480641357447470911707619766945691070098024393439617474103736912503231365532164773697023167755051595173518460579954919410967778373229665796581646513903488154256310184224190259846088000110186255550245493937113651657039447629584714548523425950428582425306083544435428212611008992863795048006894330309773217834864543113205765659868456288616808718693835297350643986297640660000723562917905207051164077614812491885830945940566688339109350944456576357666151619317753792891661581327159616877487983821820492520348473874384736771934512787029218636250627816");
		bigNumber = BigInteger.valueOf(4782);
		bigAnswer = NumberAlgorithms.getFib(bigNumber);
		assertEquals(bigCorrectAnswer, bigAnswer, "getFib BigInteger 4 failed");
	}

	@Test
	public void testGetAllFib(){
		//Test 1
		List<Integer> correctAnswer = new ArrayList<>();
		int highestNumber = 0;
		List<Integer> answer = NumberAlgorithms.getAllFib(highestNumber);
		assertEquals(correctAnswer, answer, "getAllFib int 1 failed");
		//Test 2
		correctAnswer = Arrays.asList(1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89);
		highestNumber = 100;
		answer = NumberAlgorithms.getAllFib(highestNumber);
		assertEquals(correctAnswer, answer, "getAllFib int 2 failed");
		//Test 3
		correctAnswer = Arrays.asList(1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987);
		highestNumber = 1000;
		answer = NumberAlgorithms.getAllFib(highestNumber);
		assertEquals(correctAnswer, answer, "getAllFib int 3 failed");

		//Test 4
		List<Long> longCorrectAnswer = new ArrayList<>();
		long longHighestNumber = 0;
		List<Long> longAnswer = NumberAlgorithms.getAllFib(longHighestNumber);
		assertEquals(longCorrectAnswer, longAnswer, "getAllFib long 1 failed");
		//Test 5
		longCorrectAnswer = Arrays.asList(1L, 1L, 2L, 3L, 5L, 8L, 13L, 21L, 34L, 55L, 89L);
		longHighestNumber = 100;
		longAnswer = NumberAlgorithms.getAllFib(longHighestNumber);
		assertEquals(longCorrectAnswer, longAnswer, "getAllFib long 2 failed");
		//Test 6
		longCorrectAnswer = Arrays.asList(1L, 1L, 2L, 3L, 5L, 8L, 13L, 21L, 34L, 55L, 89L, 144L, 233L, 377L, 610L, 987L);
		longHighestNumber = 1000L;
		longAnswer = NumberAlgorithms.getAllFib(longHighestNumber);
		assertEquals(longCorrectAnswer, longAnswer, "getAllFib long 3 failed");

		//Test 7
		List<BigInteger> bigCorrectAnswer = new ArrayList<>();
		BigInteger bigHighestNumber = BigInteger.ZERO;
		List<BigInteger> bigAnswer = NumberAlgorithms.getAllFib(bigHighestNumber);
		assertEquals(bigCorrectAnswer, bigAnswer, "gitAllFib BigInteger 1 failed");
		//Test 8
		bigCorrectAnswer = Arrays.asList(BigInteger.valueOf(1), BigInteger.valueOf(1), BigInteger.valueOf(2), BigInteger.valueOf(3), BigInteger.valueOf(5), BigInteger.valueOf(8), BigInteger.valueOf(13), BigInteger.valueOf(21), BigInteger.valueOf(34), BigInteger.valueOf(55), BigInteger.valueOf(89));
		bigHighestNumber = BigInteger.valueOf(100);
		bigAnswer = NumberAlgorithms.getAllFib(bigHighestNumber);
		assertEquals(bigCorrectAnswer, bigAnswer, "getAllFib BigInteger 2 failed");
		//Test 9
		bigCorrectAnswer = Arrays.asList(BigInteger.ONE, BigInteger.ONE, BigInteger.TWO, BigInteger.valueOf(3), BigInteger.valueOf(5), BigInteger.valueOf(8), BigInteger.valueOf(13), BigInteger.valueOf(21), BigInteger.valueOf(34), BigInteger.valueOf(55), BigInteger.valueOf(89), BigInteger.valueOf(144), BigInteger.valueOf(233), BigInteger.valueOf(377), BigInteger.valueOf(610), BigInteger.valueOf(987));
		bigHighestNumber = BigInteger.valueOf(1000);
		bigAnswer = NumberAlgorithms.getAllFib(bigHighestNumber);
		assertEquals(bigCorrectAnswer, bigAnswer, "getAllFib BigInteger 3 failed");
	}

	@Test
	public void testFactorial(){
		//Test 1
		Exception error = assertThrows(InvalidParameterException.class, () -> {
			NumberAlgorithms.factorial(-1);
		});
		assertEquals(NumberAlgorithms.FACTORIAL_NEGATIVE_MESSAGE, error.getMessage());
		//Test 2
		int correctAnswer = 720;
		int number = 6;
		int answer = NumberAlgorithms.factorial(number);
		assertEquals(correctAnswer, answer, "factorial int 1 failed");
		//Test 3
		correctAnswer = 479001600;
		number = 12;
		answer = NumberAlgorithms.factorial(number);
		assertEquals(correctAnswer, answer, "factorial int 2 failed");

		//Test 4
		error = assertThrows(InvalidParameterException.class, () -> {
			NumberAlgorithms.factorial(-1L);
		});
		assertEquals(NumberAlgorithms.FACTORIAL_NEGATIVE_MESSAGE, error.getMessage());
		//Test 5
		long correctAnswerLong = 720L;
		long numberLong = 6L;
		long answerLong = NumberAlgorithms.factorial(numberLong);
		assertEquals(correctAnswerLong, answerLong, "factorial long 1 failed");
		//Test 6
		correctAnswerLong = 479001600L;
		numberLong = 12L;
		answerLong = NumberAlgorithms.factorial(numberLong);
		assertEquals(correctAnswerLong, answerLong, "factorial long 2 failed");
		//Test 7
		correctAnswerLong = 2432902008176640000L;
		numberLong = 20L;
		answerLong = NumberAlgorithms.factorial(numberLong);
		assertEquals(correctAnswerLong, answerLong, "factorial long 3 failed");

		//Test 8
		final BigInteger exceptionNumberBig = BigInteger.valueOf(-1);
		error = assertThrows(InvalidParameterException.class, () -> {
			NumberAlgorithms.factorial(exceptionNumberBig);
		});
		assertEquals(NumberAlgorithms.FACTORIAL_NEGATIVE_MESSAGE, error.getMessage());
		//Test 9
		BigInteger correctAnswerBig = BigInteger.valueOf(720L);
		BigInteger numberBig = BigInteger.valueOf(6);
		BigInteger answerBig = NumberAlgorithms.factorial(numberBig);
		assertEquals(correctAnswerBig, answerBig, "factorial BigInteger 1 failed");
		//Test 10
		correctAnswerBig = BigInteger.valueOf(479001600L);
		numberBig = BigInteger.valueOf(12);
		answerBig = NumberAlgorithms.factorial(numberBig);
		assertEquals(correctAnswerBig, answerBig, "factorial BigInteger 2 failed");
		//Test 11
		correctAnswerBig = BigInteger.valueOf(2432902008176640000L);
		numberBig = BigInteger.valueOf(20L);
		answerBig = NumberAlgorithms.factorial(numberBig);
		assertEquals(correctAnswerBig, answerBig, "factorial BigInteger 3 failed");
		//Test 12
		correctAnswerBig = new BigInteger("265252859812191058636308480000000");
		numberBig = BigInteger.valueOf(30L);
		answerBig = NumberAlgorithms.factorial(numberBig);
		assertEquals(correctAnswerBig, answerBig, "factorial BigInteger 4 failed");
	}

	@Test
	public void testGCD(){
		//Test 1
		int num1 = 2;
		int num2 = 3;
		int correctAnswer = 1;
		int answer = NumberAlgorithms.gcd(num1, num2);
		assertEquals(correctAnswer, answer, "GCD int 1 failed");
		//Test 2
		num1 = 1000;
		num2 = 575;
		correctAnswer = 25;
		answer = NumberAlgorithms.gcd(num1, num2);
		assertEquals(correctAnswer, answer, "GCD int 2 failed");
		//Test 3
		num1 = 1000;
		num2 = 1000;
		correctAnswer = 1000;
		answer = NumberAlgorithms.gcd(num1, num2);
		assertEquals(correctAnswer, answer, "GCD int 3 failed");

		//Test 4
		long longNum1 = 2;
		long longNum2 = 3;
		long longCorrectAnswer = 1;
		long longAnswer = NumberAlgorithms.gcd(longNum1, longNum2);
		assertEquals(longCorrectAnswer, longAnswer, "GCD long 1 failed");
		//Test 5
		longNum1 = 1000;
		longNum2 = 575;
		longCorrectAnswer = 25;
		longAnswer = NumberAlgorithms.gcd(longNum1, longNum2);
		assertEquals(longCorrectAnswer, longAnswer, "GCD long 2 failed");
		//Test 6
		longNum1 = 1000;
		longNum2 = 1000;
		longCorrectAnswer = 1000;
		longAnswer = NumberAlgorithms.gcd(longNum1, longNum2);
		assertEquals(longCorrectAnswer, longAnswer, "GCD long 3 failed");

		//Test 7
		BigInteger bigNum1 = BigInteger.TWO;
		BigInteger bigNum2 = BigInteger.valueOf(3);
		BigInteger bigCorrectAnswer = BigInteger.ONE;
		BigInteger bigAnswer = NumberAlgorithms.gcd(bigNum1, bigNum2);
		assertEquals(bigCorrectAnswer, bigAnswer, "GCD BigInteger 1 failed");
		//Test 8
		bigNum1 = BigInteger.valueOf(1000);
		bigNum2 = BigInteger.valueOf(575);
		bigCorrectAnswer = BigInteger.valueOf(25);
		bigAnswer = NumberAlgorithms.gcd(bigNum1, bigNum2);
		assertEquals(bigCorrectAnswer, bigAnswer, "GCD BigInteger 2 failed");
		//Test 9
		bigNum1 = BigInteger.valueOf(1000);
		bigNum2 = BigInteger.valueOf(1000);
		bigCorrectAnswer = BigInteger.valueOf(1000);
		bigAnswer = NumberAlgorithms.gcd(bigNum1, bigNum2);
		assertEquals(bigCorrectAnswer, bigAnswer, "GCD BigInteger 3 failed");
	}

	@Test
	public void testToBin(){
		//Test 1
		int num = 7;
		String correctAnswer = "111";
		String answer = NumberAlgorithms.toBin(num);
		assertEquals(correctAnswer, answer, "toBin 1 failed");
		//Test 2
		num = 0;
		correctAnswer = "0";
		answer = NumberAlgorithms.toBin(num);
		assertEquals(correctAnswer, answer, "toBin 2 failed");
		//Test 3
		num = 1000000;
		correctAnswer = "11110100001001000000";
		answer = NumberAlgorithms.toBin(num);
		assertEquals(correctAnswer, answer, "toBin 3 failed");

		//Test 4
		long longNum = 7;
		correctAnswer = "111";
		answer = NumberAlgorithms.toBin(longNum);
		assertEquals(correctAnswer, answer, "toBin long 1 failed");
		//Test 5
		longNum = 0;
		correctAnswer = "0";
		answer = NumberAlgorithms.toBin(longNum);
		assertEquals(correctAnswer, answer, "toBin long 2 failed");
		//Test 6
		longNum = 1000000;
		correctAnswer = "11110100001001000000";
		answer = NumberAlgorithms.toBin(longNum);
		assertEquals(correctAnswer, answer, "toBin long 3 failed");

		//Test 7
		BigInteger bigNum = BigInteger.valueOf(7);
		correctAnswer = "111";
		answer = NumberAlgorithms.toBin(bigNum);
		assertEquals(correctAnswer, answer, "toBin big 1 failed");
		//Test 8
		bigNum = BigInteger.ZERO;
		correctAnswer = "0";
		answer = NumberAlgorithms.toBin(bigNum);
		assertEquals(correctAnswer, answer, "toBin big 2 failed");
		//Test 9
		bigNum = BigInteger.valueOf(1000000);
		correctAnswer = "11110100001001000000";
		answer = NumberAlgorithms.toBin(bigNum);
		assertEquals(correctAnswer, answer, "toBin big 3 failed");
	}
}
