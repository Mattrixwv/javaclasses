//JavaClasses/src/test/java/com/mattrixwv/generators/TestTriangularNumberGenerator.java
//Mattrixwv
// Created: 08-20-22
//Modified: 04-13-23
/*
Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.generators;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class TestTriangularNumberGenerator{
	private TriangularNumberGenerator gen;


	@BeforeEach
	public void setup(){
		gen = new TriangularNumberGenerator();
	}


	@Test
	public void testConstructor(){
		gen = new TriangularNumberGenerator();
		assertEquals(1L, gen.num);
	}

	@Test
	public void testHasNext(){
		assertTrue(gen.hasNext());

		gen.num = Long.MAX_VALUE;
		assertFalse(gen.hasNext());
	}

	@Test
	public void testNext(){
		List<Long> nums = Arrays.asList(1L, 3L, 6L, 10L, 15L, 21L, 28L, 36L, 45L);
		ArrayList<Long> generatedNums = new ArrayList<>();
		for(int cnt = 0;cnt < nums.size();++cnt){
			generatedNums.add(gen.next());
		}
		assertEquals(nums, generatedNums);

		gen.num = Long.MAX_VALUE;
		assertThrows(NoSuchElementException.class, () -> {
			gen.next();
		});
	}

	@Test
	public void testIsTriangular(){
		assertTrue(TriangularNumberGenerator.isTriangular(55L));
		assertFalse(TriangularNumberGenerator.isTriangular(54L));
	}
}
