//JavaClasses/src/test/java/mattrixwv/TestSieveOfEratosthenesBig.java
//Matthew Ellison
// Created: 04-13-23
//Modified: 04-13-23
//This class uses to Sieve of Eratosthenes to generate an infinite number of primes
/*
Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.generators;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class TestSieveOfEratostheneseBig{
	private SieveOfEratosthenesBig sieve;


	@BeforeEach
	public void setup(){
		sieve = new SieveOfEratosthenesBig();
	}


	@Test
	public void testConstructor(){
		assertEquals(BigInteger.TWO, sieve.possiblePrime);
		assertEquals(new HashMap<>(), sieve.dict);
	}

	@Test
	public void testHasNext(){
		assertTrue(sieve.hasNext());
	}

	@Test
	public void testNext(){
		List<BigInteger> correctAnswer = Arrays.asList(BigInteger.valueOf(2), BigInteger.valueOf(3), BigInteger.valueOf(5), BigInteger.valueOf(7), BigInteger.valueOf(11), BigInteger.valueOf(13), BigInteger.valueOf(17), BigInteger.valueOf(19), BigInteger.valueOf(23), BigInteger.valueOf(29), BigInteger.valueOf(31), BigInteger.valueOf(37), BigInteger.valueOf(41), BigInteger.valueOf(43), BigInteger.valueOf(47), BigInteger.valueOf(53), BigInteger.valueOf(59), BigInteger.valueOf(61), BigInteger.valueOf(67), BigInteger.valueOf(71), BigInteger.valueOf(73), BigInteger.valueOf(79), BigInteger.valueOf(83), BigInteger.valueOf(89), BigInteger.valueOf(97));
		ArrayList<BigInteger> answer = new ArrayList<>();
		for(int cnt = 0;cnt < 25;++cnt){
			BigInteger prime = sieve.next();
			answer.add(prime);
		}
		assertEquals(correctAnswer, answer);
	}
}
