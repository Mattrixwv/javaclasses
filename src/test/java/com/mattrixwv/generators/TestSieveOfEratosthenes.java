//JavaClasses/src/test/java/mattrixwv/TestSieveOfEratosthenes.java
//Matthew Ellison
// Created: 06-30-21
//Modified: 04-13-23
//This class uses to Sieve of Eratosthenes to generate an infinite number of primes
/*
Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.generators;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class TestSieveOfEratosthenes{
	private SieveOfEratosthenes sieve;


	@BeforeEach
	public void setup(){
		sieve = new SieveOfEratosthenes();
	}


	@Test
	public void testConstructor(){
		assertEquals(2L, sieve.possiblePrime);
		assertEquals(new HashMap<>(), sieve.dict);
	}

	@Test
	public void testHasNext(){
		assertTrue(sieve.hasNext());
	}

	@Test
	public void testNext(){
		List<Long> correctAnswer = Arrays.asList(2L, 3L, 5L, 7L, 11L, 13L, 17L, 19L, 23L, 29L, 31L, 37L, 41L, 43L, 47L, 53L, 59L, 61L, 67L, 71L, 73L, 79L, 83L, 89L, 97L);
		ArrayList<Long> answer = new ArrayList<>();
		for(int cnt = 0;cnt < 25;++cnt){
			long prime = sieve.next();
			answer.add(prime);
		}
		assertEquals(correctAnswer, answer);

		sieve.possiblePrime = Long.MAX_VALUE;
		sieve.dict.put(Long.MAX_VALUE, new ArrayList<>());
		sieve.dict.put(Long.MAX_VALUE + 2, new ArrayList<>());
		assertThrows(NoSuchElementException.class, () -> {
			sieve.next();
		});
	}
}
