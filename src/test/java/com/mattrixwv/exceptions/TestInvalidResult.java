//JavaClasses/src/test/java/com/mattrixwv/exceptions/TestInvalidResult.java
//Mattrixwv
// Created: 04-13-23
//Modified: 04-13-23
package com.mattrixwv.exceptions;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;


public class TestInvalidResult{
	private String message = "message";
	private Throwable cause = new Exception();


	@Test
	public void testConstructor(){
		InvalidResult exception = new InvalidResult();
		assertNull(exception.getMessage());
		assertNull(exception.getCause());

		exception = new InvalidResult(message);
		assertEquals(message, exception.getMessage());
		assertNull(exception.getCause());

		exception = new InvalidResult(cause);
		assertEquals(cause.toString(), exception.getMessage());
		assertEquals(cause, exception.getCause());

		exception = new InvalidResult(message, cause);
		assertEquals(message, exception.getMessage());
		assertEquals(cause, exception.getCause());
	}
}
