//JavaClasses/src/main/java/com/mattrixwv/generators/PentagonalNumberGenerator.java
//Mattrixwv
// Created: 08-20-22
//Modified: 08-11-24
//This class generates pentagonal numbers
/*
Copyright (C) 2024  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.generators;


import java.util.Iterator;
import java.util.NoSuchElementException;


/**
 * A generator for pentagonal numbers, which implements the {@link Iterator} interface.
 *
 * <p>
 * Pentagonal numbers are figurate numbers that represent pentagons. The n-th pentagonal number is given by
 * the formula: P(n) = (3n^2 - n) / 2.
 * </p>
 *
 * <p>
 * This generator allows iteration over pentagonal numbers starting from the first.
 * </p>
 */
public class PentagonalNumberGenerator implements Iterator<Long>{
	/**
	 * The number to generate the next pentagonal number from.
	 */
	protected long num;

	/**
	 * Constructs a new PentagonalNumberGenerator starting from the first pentagonal number.
	 */
	public PentagonalNumberGenerator(){
		num = 1L;
	}

	/**
	 * Checks if there is a next pentagonal number that can be generated without overflow.
	 *
	 * @return {@code true} if the next pentagonal number can be generated, {@code false} otherwise
	 */
	@Override
	public boolean hasNext(){
		return (3L * num * num) > num;
	}

	/**
	 * Returns the next pentagonal number.
	 *
	 * @return the next pentagonal number
	 * @throws NoSuchElementException if the next pentagonal number would cause overflow
	 */
	@Override
	public Long next(){
		if(!hasNext()){
			throw new NoSuchElementException("Number overflow");
		}

		long pentNum = ((3L * num * num) - num) / 2L;
		++num;
		return pentNum;
	}

	/**
	 * Checks if a given number is a pentagonal number.
	 *
	 * @param x the number to check
	 * @return {@code true} if the number is pentagonal, {@code false} otherwise
	 */
	public static boolean isPentagonal(Long x){
		Long n = Math.round((Math.sqrt(1.0D + (24L * x)) + 1L) / 6L);
		return (((3L * n * n) - n) / 2L) == x;
	}
}
