//JavaClasses/src/main/java/com/mattrixwv/Triple.java
//Mattrixwv
// Created: 08-20-22
//Modified: 08-11-24
//This class implements a triplet of variables
/*
Copyright (C) 2024  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv;


/**
 * A generic class representing a set of three elements.
 *
 * <p>
 * This class holds three values of potentially different types and provides methods to access them.
 * </p>
 *
 * @param <T> the type of the first element
 * @param <U> the type of the second element
 * @param <V> the type of the third element
 */
public class Triple<T, U, V>{
	/**
	 * The first element of the triple
	 */
	private T a;
	/**
	 * The second element of the triple
	 */
	private U b;
	/**
	 * The third element of the triple
	 */
	private V c;

	/**
	 * Constructs a new Triple with the specified values.
	 *
	 * @param a the first element of the triple
	 * @param b the second element of the triple
	 * @param c the third element of the triple
	 */
	public Triple(T a, U b, V c){
		this.a = a;
		this.b = b;
		this.c = c;
	}

	/**
	 * Returns the first element of the triple.
	 *
	 * @return the first element of the triple
	 */
	public T getA(){
		return a;
	}
	/**
	 * Returns the second element of the triple.
	 *
	 * @return the second element of the triple
	 */
	public U getB(){
		return b;
	}
	/**
	 * Returns the third element of the triple.
	 *
	 * @return the third element of the triple
	 */
	public V getC(){
		return c;
	}

	/**
	 * Compares the specified object with this Triple for equality.
	 *
	 * <p>
	 * Returns {@code true} if and only if the specified object is also a Triple, and all
	 * corresponding pairs of elements in the two Triples are equal.
	 * </p>
	 *
	 * @param o the object to be compared for equality with this Triple
	 * @return {@code true} if the specified object is equal to this Triple, {@code false} otherwise
	 */
	@Override
	public boolean equals(Object o){
		if(this == o){
			return true;
		}
		else if(o instanceof Triple<?, ?, ?> rightSide){
			return (a.equals(rightSide.a) && b.equals(rightSide.b) && c.equals(rightSide.c));
		}
		else{
			return false;
		}
	}

	/**
	 * Returns the hash code value for this Triple.
	 *
	 * @return the hash code value for this Triple
	 */
	@Override
	public int hashCode(){
		return a.hashCode() + b.hashCode() * c.hashCode();
	}

	/**
	 * Returns a string representation of the Triple.
	 *
	 * <p>
	 * The string representation consists of the string representations of the three elements,
	 * separated by commas and enclosed in square brackets.
	 * </p>
	 *
	 * @return a string representation of the Triple
	 */
	@Override
	public String toString(){
		return "[" + a.toString() + ", " + b.toString() + ", " + c.toString() + "]";
	}
}
