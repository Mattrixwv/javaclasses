//JavaClasses/src/main/java/mattrixwv/StringAlgorithms.java
//Matthew Ellison
// Created: 07-03-21
//Modified: 08-11-24
//This class contains algorithms for strings that I've found it useful to keep around
/*
	Copyright (C) 2024  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Utility class providing algorithms for manipulating and analyzing strings.
 *
 * <p>
 * This class offers methods to generate permutations of a string, count occurrences of a character,
 * check if a string is a palindrome, and determine if a string is pandigital.
 * </p>
 */
public class StringAlgorithms{
	/**
	 * Private constructor to prevent instantiation of this utility class.
	 */
	private StringAlgorithms(){}


	//?This is a function that creates all permutations of a string and returns a vector of those permutations.
	/**
	 * Generates all permutations of the given string.
	 *
	 * @param master the input string for which permutations are to be generated
	 * @return a list of all permutations of the input string
	 */
	public static List<String> getPermutations(String master){
		return getPermutations(master, 0);
	}
	/**
	 * Generates permutations of the given string starting from the specified index.
	 *
	 * @param master the input string for which permutations are to be generated
	 * @param num the starting index for generating permutations
	 * @return a list of permutations generated from the specified starting index
	 */
	protected static ArrayList<String> getPermutations(String master, int num){
		ArrayList<String> perms = new ArrayList<>();
		//Check if the number is out of bounds
		if((num >= master.length()) || (num < 0)){
			//Do nothing and return an empty arraylist
		}
		//If this is the last possible recurse just return the current string
		else if(num == (master.length() - 1)){
			perms.add(master);
		}
		//If there are more possible recurses, recurse with the current permutation
		else{
			ArrayList<String> temp = getPermutations(master, num + 1);
			perms.addAll(temp);
			//You need to swap the current letter with every possible letter after it
			//The ones needed to swap before will happen automatically when the function recurses
			for(int cnt = 1;(num + cnt) < master.length();++cnt){
				master = swapString(master, num, (num + cnt));
				temp = getPermutations(master, num + 1);
				perms.addAll(temp);
				master = swapString(master, num, (num + cnt));
			}

			//The array is not necessarily in alpha-numeric order. So if this is the full array sort it before returning
			if(num == 0){
				Collections.sort(perms);
			}
		}

		//Return the arraylist that was built
		return perms;
	}

	/**
	 * Swaps two characters in the given string.
	 *
	 * @param str the string in which the characters are to be swapped
	 * @param first the index of the first character to be swapped
	 * @param second the index of the second character to be swapped
	 * @return a new string with the specified characters swapped
	 */
	private static String swapString(String str, int first, int second){
		char[] tempStr = str.toCharArray();
		char temp = tempStr[first];
		tempStr[first] = tempStr[second];
		tempStr[second] = temp;

		return new String(tempStr);
	}


	//?This function returns the number of times the character occurs in the string
	/**
	 * Counts the number of occurrences of a character in a string.
	 *
	 * @param str the string in which to count occurrences
	 * @param c the character whose occurrences are to be counted
	 * @return the number of times the character occurs in the string
	 */
	public static long findNumOccurrence(String str, char c){
		return str.chars().filter(ch -> ch == c).count();
	}


	//?Returns true if the string passed in is a palindrome
	/**
	 * Checks if a string is a palindrome.
	 *
	 * @param str the string to be checked
	 * @return {@code true} if the string is a palindrome, {@code false} otherwise
	 */
	public static boolean isPalindrome(String str){
		String rev = new StringBuilder(str).reverse().toString();
		return str.equals(rev);
	}


	//?Returns true if the string passed to it is a pandigital
	/**
	 * Checks if a string is pandigital within a given range of characters.
	 *
	 * @param str the string to be checked
	 * @param bottom the starting character of the pandigital range
	 * @param top the ending character of the pandigital range
	 * @return {@code true} if the string is pandigital within the given range, {@code false} otherwise
	 */
	public static boolean isPandigital(String str, char bottom, char top){
		//Return false if top < bottom
		if(top < bottom){
			return false;
		}

		//Return false if the wrong number of characters are in the string
		if(str.length() != (top - bottom + 1)){
			return false;
		}

		//Make sure that all of the needed characters are in the string exactly one time
		for(char cnt = bottom;cnt <= top;++cnt){
			if(findNumOccurrence(str, cnt) != 1){
				return false;
			}
		}

		//If the function has reached this part it has passed all of the falsifying tests
		return true;
	}
	/**
	 * Checks if a string is pandigital with respect to the digits 1 through 9.
	 *
	 * @param str the string to be checked
	 * @return {@code true} if the string is pandigital from '1' to '9', {@code false} otherwise
	 */
	public static boolean isPandigital(String str){
		return isPandigital(str, '1', '9');
	}
}
