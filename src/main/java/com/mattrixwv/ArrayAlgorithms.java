//JavaClasses/src/main/java/mattrixwv/ArrayAlgorithms.java
//Matthew Ellison
// Created: 07-03-21
//Modified: 08-11-24
//This class contains algorithms for vectors that I've found it useful to keep around
/*
	Copyright (C) 2024  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv;


import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;


/**
 * The {@code ArrayAlgorithms} class provides static utility methods for performing
 * arithmetic operations on collections of numbers, including integers, longs, and BigIntegers.
 * It also includes utility methods for printing and converting lists.
 *
 * <p>This class is not intended to be instantiated.</p>
 */
public class ArrayAlgorithms{
	/**
	 * Private constructor to prevent instantiation of this utility class.
	 */
	private ArrayAlgorithms(){}

	//?This function returns the sum of all elements in the list
	/**
	 * Returns the sum of all elements in the given iterable collection of integers.
	 *
	 * @param nums the iterable collection of integers to sum
	 * @return the sum of all integers in the collection
	 */
	public static int getSum(Iterable<Integer> nums){
		//Setup the variables
		int sum = 0;

		//Loop through every element in the list and add them together
		for(int num : nums){
			sum += num;
		}

		//Return the sum of all elements
		return sum;
	}
	/**
	 * Returns the sum of all elements in the given iterable of longs.
	 *
	 * @param nums the iterable collection of longs to sum
	 * @return the sum of all longs in the collection
	 */
	public static long getLongSum(Iterable<Long> nums){
		//Setup the variables
		long sum = 0;

		//Loop through every element in the list and add them together
		for(long num : nums){
			sum += num;
		}

		//Return the sum of all elements
		return sum;
	}
	/**
	 * Returns the sum of all elements in the given iterable collection of {@code BigInteger} objects.
	 *
	 * @param nums the iterable collection of {@code BigInteger} objects to sum
	 * @return the sum of all {@code BigInteger} objects in the collection
	 */
	public static BigInteger getBigSum(Iterable<BigInteger> nums){
		//Setup the variables
		BigInteger sum = BigInteger.ZERO;

		//Loop through every element in the list and add them together
		for(BigInteger num : nums){
			sum = sum.add(num);
		}

		//Return the sum of all elements
		return sum;
	}

	//?This function returns the product of all elements in the list
	/**
	 * Returns the product of all elements in the given iterable collection of integers.
	 * If the iterable is empty, it returns 0.
	 *
	 * @param nums the iterable collection of integers to multiply
	 * @return the product of all integers in the collection, or 0 if the collection is empty
	 */
	public static int getProd(Iterable<Integer> nums){
		//If a blank list was passed tot he fuction return 0 as the product
		if(!nums.iterator().hasNext()){
			return 0;
		}

		//Setup the variables
		int product = 1;	//Start at 1 because x * 1 = x

		//Loop through every element in the list and multiply them together
		for(int num : nums){
			product *= num;
		}

		//Return the product of all elements
		return product;
	}
	/**
	 * Returns the product of all elements in the given iterable collection of longs.
	 * If the iterable is empty, it returns 0.
	 *
	 * @param nums the iterable collection of longs to multiply
	 * @return the product of all longs in the collection, or 0 if the collection is empty
	 */
	public static long getLongProd(Iterable<Long> nums){
		//If a blank list was passed tot he fuction return 0 as the product
		if(!nums.iterator().hasNext()){
			return 0L;
		}

		//Setup the variables
		long product = 1L;	//Start at 1 because x * 1 = x

		//Loop through every element in the list and multiply them together
		for(long num : nums){
			product *= num;
		}

		//Return the product of all elements
		return product;
	}
	/**
	 * Returns the product of all elements in the given iterable collection of {@code BigInteger} objects.
	 * If the collection is empty, it returns 0.
	 *
	 * @param nums the iterable collection of {@code BigInteger} objects to multiply
	 * @return the product of all {@code BigInteger} objects in the collection, or 0 if the collection is empty
	 */
	public static BigInteger getBigProd(Iterable<BigInteger> nums){
		//If a blank list was passed tot he fuction return 0 as the product
		if(!nums.iterator().hasNext()){
			return BigInteger.valueOf(0);
		}

		//Setup the variables
		BigInteger product = BigInteger.valueOf(1);	//Start at 1 because x * 1 = x

		//Loop through every element in the list and multiply them together
		for(BigInteger num : nums){
			product = product.multiply(num);
		}

		//Return the product of all elements
		return product;
	}

	//?Print a list
	/**
	 * Returns a string representation of the given iterable collection, with elements separated by commas
	 * and enclosed in square brackets.
	 *
	 * @param list the iterable collection to print
	 * @param <T>  the type of elements in the collection
	 * @return a string representation of the collection
	 */
	public static <T> String printList(Iterable<T> list){
		StringJoiner returnString = new StringJoiner(", ", "[", "]");
		for(T obj : list){
			returnString.add(obj.toString());
		}
		return returnString.toString();
	}

	//?Convert lists
	/**
	 * Converts a list of {@code Long} objects to a list of {@code Integer} objects.
	 *
	 * @param originalList the list of {@code Long} objects to convert
	 * @return a list of {@code Integer} objects
	 */
	public static List<Integer> longToInt(List<Long> originalList){
		ArrayList<Integer> newList = new ArrayList<>(originalList.size());
		for(Long num : originalList){
			newList.add(num.intValue());
		}
		return newList;
	}
}
