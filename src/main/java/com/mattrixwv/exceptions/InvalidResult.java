//JavaClasses/src/main/java/mattrixwv/Exceptions/InvalidResult.java
//Matthew Ellison
// Created: 08-24-20
//Modified: 08-11-24
//This is an exception for an invalid result out of one of my algorithms
package com.mattrixwv.exceptions;


/**
 * The {@code InvalidResult} class extends {@code RuntimeException} and is used to indicate
 * that an invalid result has occurred. It provides several constructors to create exceptions
 * with a custom message and/or cause.
 *
 * <p>This class is a runtime exception, meaning it is unchecked and does not need to be declared
 * in a method or constructor's {@code throws} clause.</p>
 *
 * @see RuntimeException
 */
public class InvalidResult extends RuntimeException{
	/**
	 * Constructs a new runtime exception with {@code null} as its detail message.
	 * The cause is not initialized and may subsequently be initialized by a call to {@link #initCause}.
	 */
	public InvalidResult(){
		super();
	}
	/**
	 * Constructs a new runtime exception with the specified detail message. The cause is not initialized,
	 * and may subsequently be initialized by a call to {@link #initCause}.
	 *
	 * @param msg the detail message. The detail message is saved for later retrieval by the {@link #getMessage()} method.
	 */
	public InvalidResult(String msg){
		super(msg);
	}
	/**
	 * Constructs a new runtime exception with the specified cause and a detail message of
	 * {@code (cause == null ? null : cause.toString())} (which typically contains the class
	 * and detail message of {@code cause}).
	 *
	 * @param cause the cause (which is saved for later retrieval by the {@link #getCause()} method).
	 *			A {@code null} value is permitted and indicates that the cause is nonexistent or unknown.
	 */
	public InvalidResult(Throwable cause){
		super(cause);
	}
	/**
	 * Constructs a new runtime exception with the specified detail message and cause.
	 *
	 * <p>Note that the detail message associated with {@code cause} is not automatically incorporated
	 * in this runtime exception's detail message.</p>
	 *
	 * @param msg   the detail message (which is saved for later retrieval by the {@link #getMessage()} method).
	 * @param cause the cause (which is saved for later retrieval by the {@link #getCause()} method).
	 *			A {@code null} value is permitted and indicates that the cause is nonexistent or unknown.
	 */
	public InvalidResult(String msg, Throwable cause){
		super(msg, cause);
	}
}
