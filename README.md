# JavaClasses
This is a set of classes and functions, not part of the standard library, that I have found it helpful to keep around.

# Installation
In order to use these in a program they need to be installed with maven. I normally use `mvn clean package install`. From there you can import them into a maven project using
```XML
<dependency>
	<groupId>mattrixwv</groupId>
	<artifactId>myClasses</artifactId>
	<version>1.0-SNAPSHOT</version>
</dependency>
```
Then import them into a program by `import mattrixwv.Algorithms;` or `import mattrixwv.Stopwatch;`.

# Stopwatch
This is a class that allows you to determine the execution time of an algorithm.
You can get specific resolutions of time through their own get functions, all of which return double.
However I have built it a function, getStr(), that automatically chooses the best resolution and returns both the number and resolution as a string.

Example of usage:
```Java
	Stopwatch timer = new Stopwatch();
	timer.start();
	//Code to time here
	timer.stop();
	System.out.printf("It took %s to run this algorithm", timer.getStr());
	//You could also do something like this if you prefered
	System.out.printf("It took %f milliseconds to run this algorithm", timer.getMilli());
```

# Algorithms
This is a class that contains many different algorithms that I have found it useful to keep around. This is used extensively in my ProjectEuler code found at https://bitbucket.org/Mattrixwv/ProjectEuler
All methods are overloaded to allow for using Integer, Long, and BigInteger types.
